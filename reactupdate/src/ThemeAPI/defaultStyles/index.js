import React from 'react';

export const defaultStyle = {
    color: 'black',
    background: 'white',
    fontFamily: 'Consolas',
    
};

export const buttonStyle = {
    backgroundColor: 'white',
    border: 'none',
    color: 'black',
    padding: '15px 32px',
    textAlign: 'center',
    textDecoration: 'none',
    display: 'inline-block',
    fontSize: '16px'
}

export const fontSizes = [
    '4px',
    '8px',
    '16px',
    '32px',
    '64px'
]

export const displays = [
    'none',
    'inline',
    'flex'
]

export const textDecorations = [
    'none',
    'overline',
    'line-through',
    'underline'
]

export const textAligns = [
    'center',
    'left',
    'right'
]

export const paddings = [
    '7.5px 16px',
    '15px 32px',
    '30px 64px',
    '60px 128px',

]

export const borders = [
    'dotted',
    'dashed',
    'solid',
    'double',
    'groove',
    'ridge',
    'inset',
    'outset',
    'none',
    'hidden',
]

export const colors = [
    'black',
    'blue',
    'red',
    'yellow',
    'white',
    'green',
    'orange',
    'purple',
]

export const names = {
    teng: {
        name:'Teng',
        age: 23,
        gender: 'male'
    },
    roy: {
        name: 'Roy',
        age: 18,
        gender: 'male'
    },
    sander: {
        name: 'Sander',
        age: '22',
        gender: 'male'
    }
};

export const MyContext = React.createContext();
