import React, { Component } from 'react';
import { MyContext } from '../defaultStyles';

class ContextTest extends Component {


    render() {
        return (
            <MyContext.Consumer>
                {(context) => <div>
                    
                        {Object.keys(context.buttonStyle).map((style, i) =>
                            <div key={i}>
                                <label>{style}: </label>
                                <select id="buttonStyle" name={style} onChange={context.changeStyling}>
                                    {style === "backgroundColor" && style.split("background").pop().toLowerCase()}
                                    {context[style === "backgroundColor" ? style.split("background").pop().toLowerCase() + "s" : style + "s"].map((color, i) => <option key={i} value={color}>{color}</option>)}
                                </select>
                                <br />
                            </div>)
                        }
                    
                    <br />

                </div>}
            </MyContext.Consumer>
        )
    }


}

export default ContextTest;

  //<label>{style}</label><select><option value="lol">lol</option></select>