import React, { Component } from 'react';
import './index.css';
import ContextTest from './ButtonStyle/ContextTest'
// import {MyContext, defaultStyle, names, buttonStyle, colors, backgroundColors, borders, paddings, textAligns, textDecorations, displays, fontSizes} from './providerComponent'
import * as contextThemes from './defaultStyles'

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ...contextThemes,
      name: contextThemes.names.roy,
      changeStyling: this.changeStyling
    }
  }

  changeStyling = (e) => {
    this.setState({
      [e.target.id]: {
        ...this.state[e.target.id],
        [e.target.name]: e.target.value
      }
    })
  }

  render() {
    return (
      <contextThemes.MyContext.Provider value={this.state}>
        <ContextTest />
      </contextThemes.MyContext.Provider>
    )
  }
}

export default App;
