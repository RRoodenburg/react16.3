import React, { Component } from "react";
import Slider from "./RightSlider/RightSlide/Slide";
import { RightSlideContext } from './RightSlider/Context';
import OpenModal from './Modal/ModalContent/Button/Open'
import Theme from './ThemeAPI';
import Modal from './Modal'
import LoginModal from './Modal/Views/Register'


export default class App extends Component {
    render() {

        return (

            <div style={{ display: 'flex', height: '100%' }}>
                <RightSlideContext>
                    {
                        (Context) =>
                            <div style={{ width: Context.isOpen ? '20%' : '100%', height: "100%", transition: 'width 0.3s ease', overflowY: 'auto' }}>
                                <div>
                                    <button onClick={() => Context.openSlider('lol.gif')} style={{ width: '100%' }} >Click Me!</button>
                                    <button onClick={() => Context.openSlider('high.gif')} style={{ width: '100%' }} >Click Me!</button>
                                </div>
                                <Theme />
                                <OpenModal className={"btn btn-success btn-lg"}  body={<LoginModal/>}>CLICK ME!</OpenModal>
                            </div>
                    }
                </RightSlideContext>

                <Slider />


            </div>

        )
    }
}
// <img src="https://www.imgonline.com.ua/examples/random-pixels-big.png" />