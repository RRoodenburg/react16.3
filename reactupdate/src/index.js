import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import Theme from './ThemeAPI';
import RightSlider from './RightSlider'
import Modal from './Modal'
//import Modal from './Modal'
import App from "./App.js";
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.css';
import './font-awesome-4.7.0/css/font-awesome.css';
import 'bootstrap/dist/css/bootstrap.css';



ReactDOM.render(
    <RightSlider>
        <Modal>
            <App />
        </Modal>
    </RightSlider>,
    document.getElementById('root')
);
registerServiceWorker();
