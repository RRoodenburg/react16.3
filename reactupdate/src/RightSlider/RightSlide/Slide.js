import React, { Component } from "react";
import { RightSlideContext } from '../Context';
import './Slide.css'
export default class Slide extends Component {

    render() {
        return (
            <RightSlideContext>
                {
                    (Context) =>
                        <div className={Context.isOpen ? 'openSlider' : 'closeSlider'} style={{overflowY: 'hidden'}}>
                            <div className='containers' style={{ height: "100%", width: "100%" }}>
                                <div style={{ height: "100%", width: "100%" }}>
                                    {Context.checkFileType(Context.file) === 'afbeelding' && <img src={Context.file} className='img' alt="starwars IMG" style={{ height: "100%", width: '100%' }} />}
                                    {Context.checkFileType(Context.file) === 'pdf' && <object data={Context.file} style={{ height: "100%", width: '100%' }} aria-labelledby="billing name"/>}
                                    {Context.checkFileType(Context.file) === 'office' && <iframe style={{ height: "100%", width: '100%' }} title="Office Doc" src={`https://view.officeapps.live.com/op/view.aspx?src=${Context.file}&embedded=true`}></iframe>}
                                </div>
                                <div>
                                    <button onClick={() => Context.toggleSlide(false)} className='btn closeBtn'><i className="fa fa-times fa-2x" aria-hidden="true" /></button>
                                </div>
                            </div>
                        </div>
                }
            </RightSlideContext>
        )
    }
}