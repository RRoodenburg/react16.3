import React from 'react';

export const ToggleRightSlide = {
    toggleOpen: false
}

export const RightSlideContext = React.createContext(ToggleRightSlide);