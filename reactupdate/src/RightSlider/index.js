import React, { Component } from 'react';
import * as RightSlide from './Context'

class RightSlider extends Component {
    constructor() {
        super();
        this.state = {
            isOpen: RightSlide.ToggleRightSlide.toggleOpen,
            openSlider: this.openSlider,
            checkFileType: this.checkFileType,
            toggleSlide: this.toggleSlide,
            file: ''
        }
    }

    toggleSlide = toggleSlide => {
        this.setState({
            isOpen: toggleSlide
        })
    }

    openSlider = (file) => {
        if (this.checkFileType(file) === 'download') {
            window.location = file;
            this.toggleSlide(false)
        } else {
            this.setState({ file })
            this.toggleSlide(true)
        }
    }

    checkFileType = (file) => {
        const ext = file.split(".")[file.split(".").length - 1]
        switch (ext.toLowerCase()) {
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'gif':
                return 'afbeelding'
            case 'pdf':
                return 'pdf'
            case 'docx':
            case 'doc':
            case 'docx?dl=0':
                return 'office'
            default:
                return 'download'
        }
    }

    render() {
        return (
            <RightSlide.RightSlideContext.Provider value={this.state}>
                {this.props.children}

            </RightSlide.RightSlideContext.Provider>
        )
    }
}

export default RightSlider;