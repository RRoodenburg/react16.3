import React, { Component } from "react";
import ConfirmModal from "./ModalTypes/ConfirmModal"
import MainModal from "./ModalTypes/MainModal"
import { ModalContext } from "./Context";

export default class ModalClass extends Component {

    constructor() {
        super();
        this.state = {
            main:{
                visible:false,
                body:<div>Er is iets misgegaan, meldt dit bij de beheerder</div>,
                toggle:this.toggleModal
            },
            confirm:{
                visible:false,
                toggle:this.toggleConfirm
            },
            closeAll:this.closeAll
        }
    }

    toggleConfirm = () => {
        const visible = !this.state.confirm.visible
        this.setState({
            ...this.state,
            confirm:{
                ...this.state.confirm,
                visible
            },
            // main:{
            //     ...this.state.main,
            //     visible
            // }
        })
    }

    toggleModal = body => {
        this.setState({
            main: {
                ...this.state.main,
                visible:!this.state.main.visible,
                body
            }
        });
    }

    closeAll = () =>{
        this.setState({
            main:{
                visible:false,
                toggle:this.toggleModal
            },
            confirm:{
                visible:false,
                toggle:this.toggleConfirm
            }
        })
    }

    render() {
        return (
            <ModalContext.Provider value={this.state}>
                {this.props.children}
                <MainModal {...this.state.main} />
                <ConfirmModal {...this.state.confirm} />
            </ModalContext.Provider>
        )
    }
}