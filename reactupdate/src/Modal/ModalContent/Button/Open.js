import React, { Component } from "react";
import { ModalContext } from "../../Context";
import Body from "../Body";

const Open = ({className, style, body, children}) => 
<ModalContext.Consumer>
    {
        (Context) => 
            <div onClick={() => Context.main.toggle(body)} className={className} style={style}>{children}</div>
    }
</ModalContext.Consumer>

export default Open