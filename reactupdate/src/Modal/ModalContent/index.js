import React, { Component } from "react";
import { ModalContext } from "../Context";
import Header from "./Header/index";
import Footer from "./Footer/index";
import Body from "./Body/index";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class ModalContent extends Component {

    render() {
        return (
            <ModalContext.Consumer>
                {
                    (Context) =>
                        <div>
                            <Header {...this.props}/>
                            <Body content={this.props.children}/>
                            <Footer {...this.props} />
                        </div>
                }
            </ModalContext.Consumer>
        )
    }
}

export default ModalContent
    // < div className = ' p-0  h-100 w-100 d-flex flex-column align-content-between' >
    //     <div className='float-left d-flex justify-content-between w-100 mb-2 p-1 pl-3 align-items-center header'>
    //         <Title title={this.props.title} />
    //         <Close typeModal={this.props.typeModal} />
    //     </div>

    //     <div className='container-fluid h-100' style={{ overflowY: 'auto' }}>
    //         {this.props.children}
    //     </div>

    //     <Footer {...this.props} />
    // </div >


//onClick={() => this.toggleConfirmModal(true)}
// <h2 className="alert-heading">Title</h2>
// 
// /<button onClick={() => Context.toggleConfirmationModal(true)} className='btn btn-danger mr-3'>Bevestig</button>