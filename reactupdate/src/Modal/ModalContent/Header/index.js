import React,{Component} from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { ModalContext } from "../../Context";

class Title extends Component{
    render(){
        return(
            <ModalContext>
                {
                    (Context)=><ModalHeader toggle={Context.confirm.toggle} id="close" className="header" >{this.props.title}</ModalHeader>
                
                }
            </ModalContext>
        )
    }
} 

export default Title