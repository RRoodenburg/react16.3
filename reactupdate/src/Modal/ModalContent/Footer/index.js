import React, { Component } from "react";
import { ModalContext } from "../../Context";
import '../index.css'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

const Close = ({ negativeButton, positiveButton, typeModal }) =>
    <ModalContext.Consumer>
        {
            (Context) =>
                <ModalFooter>
                    {positiveButton !== "Ja" && <Button color="primary" onClick={Context.confirm.toggle}>{positiveButton}</Button>}
                    {positiveButton === "Ja" && <Button color="primary" onClick={Context.closeAll}>{positiveButton}</Button>}
                    <Button color="secondary" onClick={Context.confirm.toggle}>{negativeButton}</Button>
                </ModalFooter>
        }
    </ModalContext.Consumer>

export default Close
    // <div className='d-flex justify-content-between pr-4 pl-4 mb-1 mt-1 footer' style={{ width: '100%' }}>
    //     {positiveButton !== "Ja" && <button onClick={Context.confirm.toggle} className='mb-3 footerBtn'>{positiveButton}</button>}
    //     {positiveButton === "Ja" && <button onClick={Context.closeAll} className='mb-3 footerBtn'>{positiveButton}</button>}
    //     <button onClick={Context.confirm.toggle} className='mb-3 footerBtn'>{negativeButton}</button>
    // </div>