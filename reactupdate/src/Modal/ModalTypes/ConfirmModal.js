import React, { Component } from "react";
import ModalContent from '../../Modal/ModalContent';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export default class ConfirmModal extends Component {
    render() {
        return (
            <Modal isOpen={this.props.visible} >
            <ModalContent typeModal="confirm" title="Weet u het zeker?" positiveButton="Ja" negativeButton="Nee">
            </ModalContent>
            </Modal>
        )
    }
}
// <Modal
//     visible={this.props.visible}
//     width="450px"
//     className="footer"
//     effect="fadeInUp"
//     onClickAway={() => true}
// >
//     <ModalContent typeModal="confirm" title="Weet u het zeker?" positiveButton="Ja" negativeButton="Nee" >
//         U kunt niet verder gaan vanwege:
//         <br/>
//             - LOL
//             <br/>
//             -NO
//             <br/>
//             - Reddit
//     </ModalContent>
// </Modal>