import React, { Component } from "react";
import ModalContent from '../../Modal/ModalContent';
import { Modal,Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
//import Modal from 'react-awesome-modal';

export default class ConfirmModal extends Component {

    render() {
        return (
            <Modal isOpen={this.props.visible} modalClassName="d-flex align-items-center" >
                <ModalContent typeModal="main" title="Voer de form in." positiveButton="Bevestig" negativeButton="Annuleren">
                    {this.props.body}
                </ModalContent>
            </Modal>
            )
        }
        
    }
//     <Form>
//     <FormGroup>
//       <Label for="exampleEmail">Email</Label>
//       <Input type="email" name="email" id="exampleEmail" placeholder="with a placeholder" />
//     </FormGroup>
//     <FormGroup>
//       <Label for="examplePassword">Password</Label>
//       <Input type="password" name="password" id="examplePassword" placeholder="password placeholder" />
//     </FormGroup>
//   </Form>